<?php

session_start();

if(!isset($_SESSION['userRole'])){
	header('Location: home.php');
}

if(isset($_POST['logout'])) {
	LogoutUser();
	
	header('Location: home.php');
}

function LogoutUser(){
	$_SESSION = array();
	
	if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000, $params["path"],
			$params["domain"], $params["secure"], $params["httponly"]
		);
	}
	
	session_destroy();
}

?>
<nav class="navbar navbar-inverse navbar-static-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="home.php" class="navbar-brand">Virtuelles Museum</a>
    </div>
    <nav class="collapse navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
        <li>
          <a href="ideas.php">Ideen</a>
        </li>
        <li>
          <a href="category.php">Gruppen</a>
        </li>
		<li>
			<a href="gallery.php">Galerie</a>
		</li>
      </ul>
	  <ul class="nav navbar-nav pull-right">
	  <?php if(isset($_SESSION['userRole']) && $_SESSION['userRole'] == 1){
				echo '<li>
						<a href="upload.php">Upload</a>
					</li>
					';
	  };
	  if(isset($_SESSION['username'])){
		  echo "<li><p class='navbar-text'>Eingeloggt als: ".$_SESSION['username']."</p></li>
				<li><form id='formLogout' method='post'><input name='logout' type='hidden' id='logout' value='logout'><button type='submit' id='btnLogout' class='btn btn-default navbar-btn' style='margin-right: 5px'>Logout</button></form></li>";
	  };
	  ?>
	  </ul>
	  
    </nav>
  </div>
</nav>