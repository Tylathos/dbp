<?php
    include ('DatabaseConnection.php');
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title><?php echo $Name; ?></title>
    <style>
        body, html{
            background-color:#EADDCA;
            position: absolute;
            left: 0%;
            top: 0%;
            height: 100%;
            width: 100%;
            border: 0px;
            margin-bottom: 0px;
            margin-top: 0px;
            margin-left: 0px;
            margin-right: 0px;
        }

        #Bild{
            position:absolute;
            top:0%;
            width: 100%;
            height: 100%;
            display:table;
            border:none;
        }

        #Zitat{
            position:absolute;
            top:0%;
            left:0%;
            width:500px;
            height: 67%;
            display:table;
            border:none;
        }

        #Zitat p{
            font-size: 48px;
            text-align: center;
            vertical-align: middle;
            display:table-cell;
        }

        #Text1_rand{
            background-color:#C4151B;
            position:absolute;
            left:0%;
            width:17%;
            height:1%;
        }

        #Text1{
            padding: 5px;
            position:absolute;
            top:68%;
            left:0%;
            width:calc(17% - 10px);
            height:calc(28% - 10px);
            color: white;
            text-align: center;
            vertical-align: middle;
        }

        #Text2{
            padding: 5px;
            position:absolute;
            top:68%;
            left:17%;
            width:calc(27% - 10px);
            height:calc(28% - 10px);
            text-align: center;
            vertical-align: middle;
        }

        #Text2_rand{
            background-color:#DE761B;
            position:absolute;
            left:17%;
            width:27%;
            height:1%;
        }

        #Text3{
            padding: 5px;
            position:absolute;
            top:68%;
            left:44%;
            width:calc(12% - 10px);
            height:calc(28% - 10px);
        }

        #Text3_rand{
            background-color:#AA9A00;
            position:absolute;
            left:44%;
            width:12%;
            height:1%;
        }

        #Text4{
            text-align: center;
            vertical-align: middle;
            opacity: 0.1;
            position:absolute;
            top:68%;
            left:56%;
            width:44%;
            height:28%;
        }

        #text4visible {
            padding: 5px;
            text-align: center;
            position:absolute;
            color: white;
            top:68%;
            left:56%;
            width:calc(44% - 10px);
            height:calc(28% - 10px);
        }

        #Text4_rand{
            opacity: 0.5;
            background-color:#7A6F27;
            position:absolute;
            left:56%;
            width:44%;
            height:1%;
        }

        #info{
            vertical-align: central;
            position:absolute;
            top:97%;
            left:0%;
            height:3%;
            width:100%;
            background-color: white;
            color: red;
        }

    </style>
</head>
<body>
<div id="rahmen">
    <div id="Bild">
        <img src="<?php print $Bild1; ?>" style="z-index:5; max-height: 100%; " align="right"; class="img-responsive">
        <div id="Zitat" style="z-index:1;"><p><?php echo $Zitat; ?></p></div>
    </div>
    <div id="Text1_rand" style="top:67%;"></div>
    <div id="Text1" style="background-color:#505E65;">
        <?php
        $u = 0;
        for($u = 0; $u < count($Literatur); $u++) {
            echo $Literatur[$u]."<br/>";
        }
        ?>
    </div>
    <div id="Text1_rand" style="top:96%;"></div>
    <div id="Text2_rand" style="top:67%;"></div>
    <div id="Text2" style="background-color:#E5A810;"><?php echo $Wirkenk; ?></div>
    <div id="Text2_rand" style="top:96%;"></div>
    <div id="Text3_rand" style="top:67%;"></div>
    <div id="Text3" style="background-color:#DE761B;"><img src="<?php print $Bild2; ?>" class="img-responsive"></div>
    <div id="Text3_rand" style="top:96%;"></div>
    <div id="Text4_rand" style="top:67%;"></div>
    <div id="Text4" style="background-color:white;"></div>
    <div id="text4visible"><?php echo $Biok ?></div>
    <div id="Text4_rand" style="top:96%;"></div>
</div>
<div id="info"><?php echo $Name." *".$Gdatum." in ".$Gort." &dagger;".$Tdatum." in ".$Tort; ?></div>
</body>
</html>