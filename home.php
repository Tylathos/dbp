﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Virtuelles Museum - Home</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="home.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
  <?php
 session_start();
  
  if(isset($_POST['username']) && $_POST['password']){
    $userRole = CheckCredentials($_POST['username'],$_POST['password']);

    if($userRole != ''){
        SetSessionData($userRole,$_POST['username']);
    }
} else if(isset($_POST['logout'])) {
	LogoutUser();
}

function LogoutUser(){
	$_SESSION = array();
	
	if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000, $params["path"],
			$params["domain"], $params["secure"], $params["httponly"]
		);
	}
	
	session_destroy();
}

function SetSessionData($userRole,$username){
     session_regenerate_id();

    $_SESSION['userRole'] = $userRole;
    $_SESSION['username'] = $username;
}

function CheckCredentials($username,$password){

    $SQL_USERNAME = 'it';
    $SQL_PW = 'neketa47';
    $SQL_SERVER = 'localhost';
    $SQL_DB = 'vimu';

    $connection = new mysqli($SQL_SERVER,$SQL_USERNAME,$SQL_PW,$SQL_DB);

    if($connection->connect_error){
        die("Da ist ein Fehler!");
    }

    $query = "SELECT UsrId,Role,UsrPass FROM user WHERE UsrName = '".$username."'";
    $query_result = $connection->query($query);
    if($query_result->num_rows == 1){
        $userData = $query_result->fetch_assoc();
        if(md5($password) == $userData['UsrPass']){
            return $userData['Role'];
        } else {
            return '';
        }
    }
}
?>
  <div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top">
      
        <div class="navbar-header">
	    <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </a>
        <a class="navbar-brand" href="home.php">Virtuelles Museum</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><?php if(isset($_SESSION['userRole'])){ echo '<a href="ideas.php">Ideen</a>';}?></li>
            <li><?php if(isset($_SESSION['userRole'])){ echo '<a href="category.php">Gruppen</a>';}?></li>
			<li><?php if(isset($_SESSION['userRole'])){ echo '<a href="gallery.php">Galerie</a>';}?></li>
		 
		 </ul> 
		 <ul class="nav navbar-nav pull-right">
		 <?php 
			if(isset($_SESSION['userRole']) && $_SESSION['userRole'] == 1){
				echo '
						<li>
						<a href="upload.php">Upload</a>
						</li>
					';
			};
			if(!isset($_SESSION['username'])){ 
			echo '
			<li class="dropdown" id="menuLogin">
				<a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
				<div class="dropdown-menu" style="padding:17px;">
              <form class="form" id="formLogin" method="post"> 
                <input name="username" id="username" type="text" placeholder="Username"> 
                <input name="password" id="password" type="password" placeholder="Password"><br>
                <button type="submit" id="btnLogin" class="btn" >Login</button>
              </form>
            </div>
          </li>';
			} else {
				echo "<li><p class='navbar-text'>Eingeloggt als: ".$_SESSION['username']."</p></li>
				<li><form id='formLogout' method='post'><input name='logout' type='hidden' id='logout' value='logout'><button type='submit' id='btnLogout' class='btn btn-default navbar-btn' style='margin-right: 5px'>Logout</button></form></li>";
			}
			?>
          </ul>
        </div>

    </div>
  </div><!-- /container -->
</div><!-- /navbar wrapper -->
<?php 
function toogleLink($target){
		if(isset($_SESSION['userRole'])){
			echo 'href="'.$target.'"';
		} else {
			echo 'href="#" onClick="alert(\'Bitte loggen Sie sich ein um auf die Seite zuzugreifen!\');"';
		}
}
?>
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="item active">
      <img src="Pictures/lightbulb.jpeg" style="width:100%" class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          <h1>Z&uuml;ndende Ideen</h1>
          <p></p>
          <p><a class="btn btn-lg btn-primary" <?php toogleLink("ideas.php");?>>Mehr erfahren</a>
        </p>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="Pictures/clock.jpg" class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          <p>Inspiration aus vielen Epochen</p>
		  <p><a class="btn btn-lg btn-primary" <?php toogleLink("category.php");?>>Mehr erfahren</a>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="Pictures/kid.jpg" class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          <p>Die gr&ouml;&szlig;ten Pers&ouml;nlichkeiten der Sozialp&auml;dagogik</p>
          <p><a class="btn btn-large btn-primary" <?php toogleLink("gallery.php");?>>Galerie ansehen</a></p>
        </div>
      </div>
    </div>
  </div>
  <!-- Controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="icon-prev"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="icon-next"></span>
  </a>  
</div>
<!-- /.carousel -->


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container marketing">

  <!-- Three columns of text below the carousel -->
  <div class="row">
    <div class="col-md-4 text-center">
      <img class="img-circle" src="Pictures/pfeil.jpg">
      <h2>Den Rundgang beginnen</h2>
      <p>Das virtuelle Museum selbst erkunden</p>
      <p><a class="btn btn-default" <?php toogleLink("gallery.php",1);?>>Los geht's »</a></p>
    </div>
    <div class="col-md-4 text-center">
      <img class="img-circle" src="http://placehold.it/140x140">
      <h2>Test&uuml;berschrift</h2>
      <p>TextText TextText TextText TextText</p>
      <p><a class="btn btn-default" href="#">View details »</a></p>
    </div>
    <div class="col-md-4 text-center">
      <img class="img-circle" src="Pictures/logo.jpg">
      <h2>Studienprojekt</h2>
      <p>Mehr &uuml;ber diese Kollaboration zwischen den Fakult&auml;ten Sozialwissenschafen und Technik erfahren</p>
      <p><a class="btn btn-default" href="#">Mehr erfahren »</a></p>
    </div>
  </div><!-- /.row -->

</div><!-- /.container -->
</body>
</html>