<?php
include ('DatabaseConnection.php');
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title><?php echo $Name; ?></title>
    <style type="text/css">

        body, html {
            width: 100%;
            height:100%;
            position: absolute;
            left:0%;
            top:0%;
            font-size:small;
            margin:0px;
			color: white;
        }

        #Zitat{
            position:absolute;
            left:0%;
            top:0%;
            width:33%;
            height:73%;
            background-color:#2E3B40;
			display:table;
        }
		
		#Zitat p{
            font-size: 48px;
            text-align: center;
            vertical-align: middle;
            display:table-cell;
        }
		
        #Portrait{

            position:absolute;
            left:33%;
            top:0%;
            width:33%;
            height:73%;
            background-color:white;
        }
		
        #Bild2{

            position:absolute;
            left:66%;
            top:0%;
            margin-right:0;
            width:34%;
            height:73%;
            background-color:white;
        }
		
		#LiteraturRandOben{
			position:absolute;
			top:73%;
			left:0%;
			height:1%;
			width: 20%;
			background-color: #9E458A;
		}

        #Literatur{
            position:absolute;
            top:74%;
            left:0%;
            height:23%;
            width: 20%;
            background-color: #2E3B40;
			display:table;
        }
		
		#Literatur p{
			display:table-cell;
            font-size: 15px;
            text-align: center;
            vertical-align: middle;
		}
		
		#LiteraturRandUnten{
			position:absolute;
			top:97%;
			left:0%;
			height:1%;
			width: 20%;
			background-color: #FA0000;
		}

		#WirkenRandOben{
			position:absolute;
			top:73%;
			left:20%;
			height:1%;
			width: 40%;
			background-color: #FF5367;
		}
		
        #Wirken{
            position:absolute;
            left: 20%;
            top: 74%;
            width: 40%;
            height: 23%;
            vertical-align: top;
            background-color:#DE1C00;
			display: table;
        }
		
		#Wirken p{
			display:table-cell;
            font-size: 15px;
            vertical-align: middle;
			text-align: center;
		}
		
		#WirkenRandUnten{
			position:absolute;
			top:97%;
			left:20%;
			height:1%;
			width: 40%;
			background-color: #DA0000;
		}
		
		#BioRandOben{
			position:absolute;
			top:73%;
			left:60%;
			height:1%;
			width: 40%;
			background-color: #B35EA1;
		}

        #Bio{
            position:absolute;
            left:60%;
            top:74%;
            width: 40%;
            height: 23%;
            vertical-align: top;
            background-color:#C70000;
			display:table;
        }
		
		#Bio p{
			display:table-cell;
            font-size: 15px;
            text-align: center;
            vertical-align: middle;
		}
		
		#BioRandUnten{
			position:absolute;
			top:97%;
			left:60%;
			height:1%;
			width: 40%;
			background-color: #DA0000;
		}
		
		#Person{
			position:absolute;
			left:0%;
			top:98%;
			color: #E7718F;
			
		}

    </style>
</head>
<body>
<div id="Zitat"><p><?php echo $Zitat; ?></p></div>
<div id="Portrait"><img style="width:100%; height:100%;" src="<?php echo $Bild1; ?>"/></div>
<div id="Bild2"><img style="width:100%; height:100%;" src="<?php echo $Bild2; ?>"/></div>
<div id="LiteraturRandOben"></div>
<div id="Literatur"><p>	
		<?php
			$u = 0;
			for($u = 0; $u < count($Literatur); $u++) {
				echo $Literatur[$u]."<br/>";
			}
		?>
		</p>
</div>
<div id="LiteraturRandUnten"></div>
<div id="BioRandOben"></div>
<div id="Bio"><p><?php echo $Biok; ?></p></div>
<div id="BioRandUnten"></div>
<div id="WirkenRandOben"></div>
<div id="Wirken"><p><?php echo $Wirkenk; ?></p></div>
<div id="WirkenRandUnten"></div>
<div id="Person"><?php echo $Name." *".$Gdatum." in ".$Gort." &dagger;".$Tdatum." in ".$Tort; ?></div>
</body>
</html>