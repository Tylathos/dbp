drop table if exists user;
drop table if exists rvp;
drop table if exists rpl;
drop table if exists rpi;
drop table if exists rpg;
drop table if exists rbp;
drop table if exists person;
drop table if exists literatur;
drop table if exists ideen;
drop table if exists gruppen;
drop table if exists bild;
drop table if exists video;

CREATE TABLE vimu.user(
UsrID int Primary Key AUTO_INCREMENT,
UsrName varchar(255) not null,
UsrPass varchar(255) not null,
Email varchar(255) not null,
Role int not null
);

CREATE TABLE vimu.person(
Pid int Primary Key AUTO_INCREMENT,
Name tinytext,
Wirkenl text,
Wirkenk text,
Biol text,
Biok text,
Gdatum date,
Gort varchar(255),
Tdatum date,
Tort varchar(255),
Zitat text,
Pattern int
);

CREATE TABLE vimu.literatur(
Lid int Primary Key AUTO_INCREMENT,
Titel varchar(255),
Herausgeber varchar(255),
Jahr int,
Auflage int,
URL text,
Datum date
);

CREATE TABLE vimu.gruppen(
Gid int Primary Key AUTO_INCREMENT,
Name tinytext,
Kurzbeschreibung text,
Zeit tinytext,
Beschreibung text,
Errungenschaften text
);

CREATE TABLE vimu.ideen(
Iid int Primary Key AUTO_INCREMENT,
Name tinytext,
Kurzbeschreibung text,
Beschreibung text,
Errungenschaften text
);

CREATE TABLE vimu.bild(
Bid int Primary Key AUTO_INCREMENT,
Titel tinytext,
Beschreibung text,
Data tinytext,
position int not null
);

CREATE TABLE vimu.video(
Vid int Primary Key AUTO_INCREMENT,
Titel tinytext,
Beschreibung text,
Data text
);

CREATE TABLE vimu.rbp(
	Bid int,
	Pid int,
	FOREIGN KEY(Bid)
		REFERENCES bild(Bid),
	FOREIGN KEY(Pid)
		REFERENCES person(Pid)
);

CREATE TABLE vimu.rvp(
	Vid int,
	Pid int,
	FOREIGN KEY(Vid)
		REFERENCES video(Vid),
	FOREIGN KEY(Pid)
		REFERENCES person(Pid)
);

CREATE TABLE vimu.rpl(
	Pid int,
	Lid int,
	FOREIGN KEY(Pid)
		REFERENCES person(Pid),
	FOREIGN KEY(Lid)
		REFERENCES literatur(Lid)
);

CREATE TABLE vimu.rpi(
	Pid int,
	Iid int,
	FOREIGN KEY(Pid)
		REFERENCES person(Pid),
	FOREIGN KEY(Iid)
		REFERENCES ideen(Iid)
);

CREATE TABLE vimu.rpg(
	Pid int,
	Gid int,
	FOREIGN KEY(Pid)
		REFERENCES person(Pid),
	FOREIGN KEY(Gid)
		REFERENCES gruppen(Gid)
);