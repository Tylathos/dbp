﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Virtuelles Museum - Inhalt erstellen</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script language="javascript">
		var AngezeigteBreite = 500;
		var GespeicherteBreite = 0;

		function BildAnpassen(Bild)
		{
			var bigpic = document.getElementById("bigpic");
			if(bigpic.style.display == "none") {
				bigpic.children[0].src = Bild.src;
				bigpic.style.display = "block";
			}
			else{
				bigpic.style.display = "none";
			}
		}
		
		function add_literature(){
			var liste = document.getElementById("literaturliste");
			liste.innerHTML += "<li><input name='literatur[]'></li>";
		}
	</script>
	<style>
	    body{ 
	        background-image: url(Pictures/background.png);
			background-size:100%;
	    }
		fieldset label{
			padding:5px;
		}
	</style>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </head>
	<body>
<?php 
include('menu.php');
if(isset($_SESSION['userRole']) && $_SESSION['userRole'] != 1){
	header('Location:home.php');
}
?>
		<div>
            <form name="form" action="" method="post" enctype="multipart/form-data">
                <table class="table" style="width:70%; margin-left:15%;">
                    <tr>
                        <td>Name</td>
                        <td colspan="2"><input type="text" name="name" class="form-control" /></td>
                    </tr>
                    <tr>
                        <td>Geburt</td>
                        <td><input type="text"  name="gdatum"  class="form-control" placeholder="Datum YYYY-MM-DD" /></td>
                        <td><input type="text"  name="gort"  class="form-control" placeholder="Ort" /></td>
                    </tr>
                    <tr>
                        <td>Tod</td>
                        <td><input type="text"  name="tdatum"  class="form-control" placeholder="Datum YYYY-MM-DD" /></td>
                        <td><input type="text"  name="tort"  class="form-control" placeholder="Ort" /></td>
                    </tr>
                    <tr>
                        <td>Zitat</td>
                        <td colspan="2"><input type="text" name="zitat"  class="form-control" /></td>
                    </tr>
                    <tr>
                        <td>Biographie Kurz</td>
                        <td colspan="2"><textarea type="text" name="biok" class="form-control"></textarea></td>
                    </tr>
                    <tr>
                        <td>Biographie Lang</td>
                        <td colspan="2"><textarea type="text" name="biol" rows="4" class="form-control"></textarea></td>
                    </tr>
                    <tr>
                        <td>Wirken Kurz</td>
                        <td colspan="2"><textarea type="text" name="wirkenk" class="form-control"></textarea></td>
                    </tr>
                    <tr>
                        <td>Wirken Lang</td>
                        <td colspan="2"><textarea type="text" name="wirkenl" rows="4" class="form-control"></textarea></td>
                    </tr>
					<tr>
						<td>Literatur</td>
						<td>
							<input type="button" onClick="add_literature()" value="Literatur Hinzufügen"/>
							<ul id="literaturliste" style="margin-top:3px;">
								
							</ul>
						</td>
					</tr>
					<tr>
						<td>Ideen</td>
						<td>
							<fieldset>
							<?php
								$servername = "localhost";
								$username = "it";
								$password = "neketa47";
								$dbname = "vimu";
								$bid = 0;
								$pid = 0;

								// Create connection
								$conn = new mysqli($servername, $username, $password, $dbname);
								// Check connection
								if ($conn->connect_error) {
									die("Connection failed: " . $conn->connect_error);
								}
								
								$ideen = $conn->query("SELECT iid, name from ideen");
								$ideen->data_seek(0);
								while ($row = $ideen->fetch_assoc()){
									echo '<label for='.$row['iid'].'><input type="checkbox" name="ideen[]" value="'.$row['iid'].'">'.$row['name'].'</label>';
								}
							?>
							</fieldset>
						</td>
					</tr>
					<tr>
						<td>Gruppen</td>
						<td>
							<fieldset>
							<?php
								$servername = "localhost";
								$username = "it";
								$password = "neketa47";
								$dbname = "vimu";
								$bid = 0;
								$pid = 0;

								// Create connection
								$conn = new mysqli($servername, $username, $password, $dbname);
								// Check connection
								if ($conn->connect_error) {
									die("Connection failed: " . $conn->connect_error);
								}
								
								$ideen = $conn->query("SELECT gid,name from gruppen");
								$ideen->data_seek(0);
								while ($row = $ideen->fetch_assoc()){
									echo '<label for='.$row['gid'].'><input type="checkbox" name="gruppen[]" value="'.$row['gid'].'">'.$row['name'].'</label>';
								}
							?>
							</fieldset>
						</td>
					</tr>
                    <tr>
                        <td>Porträt<br/>(Bild 1)</td>
                        <td colspan="2"><input type="file" id="portrait" name="portrait" /></td>
                    </tr>
                    <tr>
                        <td>Bild 2</td>
                        <td colspan="2"><input type="file" name="bild" /></td>
                    </tr>
                    <tr>
                        <td>Anzeige-Format</td>
                        <td colspan="2">
							<p>Klicken Sie auf ein Bild um es genau zu betrachten!</p>
							<p>Achten Sie bei der Auswahl des Formats darauf, dass die hochgeladenen Bilder ungefähr zu den vorgegebenen Bereichen des Formats passen, da diese sonst gestreckt werden!</p>
							<fieldset>
								<input type="radio" name="pattern" value="1"><img onClick="BildAnpassen(this)" src="Pictures/montessori_template.png" height="70px"/>
								<input type="radio" name="pattern" value="2"><img onClick="BildAnpassen(this)" src="Pictures/cohn_template.png" height="70px"/><br/><br/>
								<input type="radio" name="pattern" value="3"><img onClick="BildAnpassen(this)" src="Pictures/pappenheim_template.png" height="70px"/>
								<input type="radio" name="pattern" value="4"><img onClick="BildAnpassen(this)" src="Pictures/template4.png" height="70px"/>
							</fieldset>
						 </td>
                    </tr>
					<tr>
						<td><input type="submit" name="submit" value="Abschicken" /></td>
						<td colspan="2">
							<?php
								$servername = "localhost";
								$username = "it";
								$password = "neketa47";
								$dbname = "vimu";
								$bid = 0;
								$pid = 0;

								// Create connection
								$conn = new mysqli($servername, $username, $password, $dbname);
								// Check connection
								if ($conn->connect_error) {
									die("Connection failed: " . $conn->connect_error);
								}

								if(isset($_POST['submit']))
								{
									if (empty($_POST['ideen']) || empty($_POST['gruppen']) || empty($_POST['name']) || empty($_POST['wirkenl']) || empty($_POST['wirkenk']) || empty($_POST['biol']) || empty($_POST['biok']) || empty($_POST['gdatum']) || empty($_POST['gort']) || empty($_POST['zitat']) || empty($_POST['pattern']) || !isset($_FILES['portrait']) || !isset($_FILES['bild'])) {
										die("Bitte überprüfen Sie, ob alle Pflichtfelder ausgefüllt sind und 2 Bilder hochgeladen wurden!");
									}
									$name = $_POST['name'];
									$wirkenl = $_POST['wirkenl'];
									$wirkenk = $_POST['wirkenk'];
									$biol = $_POST['biol'];
									$biok = $_POST['biok'];
									$gdatum = (strlen($_POST['gdatum'])>0)?"'".$_POST['gdatum']."'":'null';
									$gort = $_POST['gort'];
									$tdatum = (strlen($_POST['tdatum'])>0)?"'".$_POST['tdatum']."'":'null';
									$tort = $_POST['tort'];
									$zitat = $_POST['zitat'];
									$pattern = $_POST['pattern'];
									
									$person = "INSERT INTO person VALUES(null, '$name', '$wirkenl', '$wirkenk', '$biol', '$biok', $gdatum, '$gort', $tdatum, '$tort', '$zitat', $pattern);";
									if ($conn->query($person) === TRUE) {
										$pid = $conn->insert_id;
									}
									else {
										echo "Error: " . $person . "<br>" . $conn->error;
									}									
									
									$imagename = $_FILES['portrait']['name'];
									$imagetemp = $_FILES['portrait']['tmp_name'];
									$imagePath = $_SERVER['DOCUMENT_ROOT'] . "/Datenbanken/Pictures/";

									if(is_uploaded_file($imagetemp)) {
										if(move_uploaded_file($imagetemp, $imagePath . $imagename)) {
											$bild = "INSERT INTO bild VALUES(null, '$name', null, '../Pictures/$imagename', 1);";
											if ($conn->query($bild) === TRUE) {
												$bid = $conn->insert_id;
												$bild = "INSERT INTO rbp VALUES($bid, $pid);";
												$conn->query($bild);
											}
											else {
												echo "Error: " . $bild . "<br>" . $conn->error;
											}
										}
										else {
											echo "Porträt konnte nicht gespeichert werden!";
										}
									}
									else {
										echo "Porträt wurde nicht hochgeladen!";
									}									
									
									$imagename = $_FILES['bild']['name'];
									$imagetemp = $_FILES['bild']['tmp_name'];

									if(is_uploaded_file($imagetemp)) {
										if(move_uploaded_file($imagetemp, $imagePath . $imagename)) {
											$bild = "INSERT INTO bild VALUES(null, '$name', null, '../Pictures/$imagename', 2);";
											if ($conn->query($bild) === TRUE) {
												$bid = $conn->insert_id;
												$bild = "INSERT INTO rbp VALUES($bid, $pid);";
												$conn->query($bild);
											}
											else {
												echo "Error: " . $bild . "<br>" . $conn->error;
											}
										}
										else {
											echo "Bild 2 konnte nicht gespeichert werden!";
										}
									}
									else {
										echo "Bild 2 wurde nicht hochgeladen!";
									}
									
									foreach($_POST['ideen'] as $ideen) {
										$conn->query("INSERT INTO rpi VALUES($pid, $ideen)");
									}
									
									foreach($_POST['gruppen'] as $gruppen) {
										$conn->query("INSERT INTO rpg VALUES($pid, $gruppen)");
									}
									
									foreach($_POST['literatur'] as $index => $value) {
										if (!is_null($value)) {
											$literatur = "INSERT INTO Literatur VALUES(null, '$value', null, null, null, null, null);";
											if ($conn->query($literatur) === TRUE) {
												$lid = $conn->insert_id;
												$conn->query("INSERT INTO rpl VALUES($pid, $lid);");
											}
										}
									}
								}

								$conn->close();
							?>
						</td>
					</tr>
                </table>
            </form>
		</div>
		<div id="bigpic" style="display:none; z-index:1; position:absolute; left:10%; top:30%; max-width:80%; max-height:60%;"><img style="max-width:100%; max-height:100%;" onClick="BildAnpassen(this)" /></div>
	</body>
</html>