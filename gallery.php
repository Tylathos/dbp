<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Virtuelles Museum - Gallerie</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<style>
		body{
			background: url(Pictures/background.png) no-repeat center center fixed;
		}
		div{
			max-width:98%;
		}
		img{
			margin-right:0px;
		}
	</style>
  </head>
  <body>
<?php include('menu.php');?>
		<?php
			$servername = "localhost";
			$username = "it";
			$password = "neketa47";
			$dbname = "vimu";

			// Create connection
			$conn = new mysqli($servername, $username, $password, $dbname);
			// Check connection
			if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
			}
			
			$zähler = 0;
			$abfrage = "SELECT person.pid as pid, name, data FROM person inner join rbp on person.pid = rbp.pid inner join bild on rbp.bid = bild.bid where mod(bild.bid, 2) = 0";
			$ergebnis = $conn->query($abfrage);
			if (!$ergebnis) {
				die('Ungültige Abfrage: ' . $conn->error);
			}
			
			$ergebnis->data_seek(0);
			while ($row = $ergebnis->fetch_assoc())
		   {
				if($zähler == 0){
					echo "<div class='row' style='margin-left:2%;'>";
					echo '<div class="col-md-4">';
					echo '<a href="display.php?id='.$row['pid'].'"><img src="'.$row['data'].'" class="img-responsive"></a>';
					echo $row['name'];
					echo "</div>";
				}
				else if($zähler == 2){
					echo "<div class='col-md-4'>";
					echo '<a href="display.php?id='.$row['pid'].'"><img src="'.$row['data'].'" class="img-responsive"></a>';
					echo $row['name'];
					echo '</div>';
					echo "</div>";
					echo "<hr>";
					$zähler = -1;
				}
				else{
					echo "<div class='col-md-4'>";
					echo '<a href="display.php?id='.$row['pid'].'"><img src="'.$row['data'].'" class="img-responsive"></a>';
					echo $row['name'];
					echo "</div>";
				}
				$zähler = $zähler + 1;
		   }
		   $ergebnis->close();
		   if($zähler != 0){
				echo '</div>';
		   }
			$conn->close();
		?>
</body>
</html>