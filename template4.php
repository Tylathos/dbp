<?php
    include ('DatabaseConnection.php');
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title><?php echo $Name; ?></title>
    <style type="text/css">
    #Bild1{
	position:absolute;
	top:0%;
	left:33%;
	width:33%;
	height: 75%;
	display:table;
	border:none;
}

#Text4{
	
	position:absolute;
	top:0%;
	left:65%;
	width:35%;
	height:33%;
	display:table;
}

#Text4_rand{
	background-color:#234567;
	position:absolute;
	left:65%;
	top:33%;
	width:35%;
	height:3%;
	display:table;
	
}

#Text5{
	position:absolute;
	top:36%;
	left:65%;
	width:35%;
	height:39%;
	display:table;
	
}

#Text1{
	position:absolute;
	top:0%;
	left:0%;
	width:33%;
	height:33%;
	display:table;
}

#Text1_rand{
	background-color:#234567;
	position:absolute;
	left:0%;
	top:33%;
	width:33%;
	height:3%;
	display:table;
}

#Text2{
	position:absolute;
	top:36%;
	left:0%;
	width:33%;
	height:17%;
	display:table;
}

#Text2_rand{
	background-color:gray;
	position:absolute;
	top:53%;
	left:0%;
	width:33%;
	height:3%;
	display:table;
}

#Text3{
	position:absolute;
	top:56%;
	left:0%;
	width:33%;
	height:16%;
	display:table;
}

#Text3_rand{
	background-color:green;
	position:absolute;
	top:72%;
	left:0%;
	width:33%;
	height:3%;
	display:table;
}

#Bild2{
	position:absolute;
	top:75%;
	left:0%;
	width:33%;
	height:25%;
	display:table;
	background-color:purple;
}


#Zitat{
	position:absolute;
	top:75%;
	left:33%;
	width:67%;
	height: 25%;
	display:table;
	border:none;
	display:table;
}

#Zitat p{
	font-size: 48px;
	text-align: center;
	vertical-align: middle;
	display:table-cell;
}
#Rand_unten{
	background-color:red;
	width:100%;
	height:10px;
	display:table;
}
#info{
	position:absolute;
	top:95%;
	left:0%;
	height:5%;
	width:100%;
	background-color:#193710
}
    </style>
</head>
<body>
<div id="rahmen">
    <div id="Text1" style="background-color:green;"><?php echo $Wirkenl; ?></div>
    <div id="Bild1" style="background-color:yellow;text-align:center;"><img style="width:100%; height:100%;" src="<?php echo $Bild1; ?>"/></div>
	<div id="Text4" style="background-color:pink;"><?php echo $Wirkenk; ?></div>
    <div id="Text1_rand"></div>
	<div id="Text4_rand"></div>
	<div id="Text5" style="background-color: green;"><?php echo $Biol; ?></div>
    <div id="Text2" style="background-color:black; color:white;">
		<?php
			$u = 0;
			for($u = 0; $u < count($Literatur); $u++) {
				echo $Literatur[$u]."<br/>";
			}
		?>
	</div>
    <div id="Text2_rand"></div>
    <div id="Text3" style="background-color:#234567;"><?php echo $Biok; ?></div>
    <div id="Text3_rand"></div>
    <div id="Bild2" style="text-align:center;"><img style="width:100%; height:100%;" src="<?php echo $Bild2; ?>"/></div>
    <div id="Zitat"><p><?php echo $Zitat; ?></p></div>
</div>
<div id="Rand_unten"></div>
<div id="info"><?php echo "*"; echo $Gdatum; echo " in "; echo $Gort; echo "   + "; echo $Tdatum; echo " in "; echo $Tort; ?></div>
</body>
</html>