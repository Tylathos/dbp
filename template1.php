<?php
	include ('DatabaseConnection.php');
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title><?php echo $Name; ?></title>
    <style type="text/css">
        #zitat{
            position:absolute;
            top:0%;
            left:0%;
            width:calc(22% - 20px);
            height:100%;
            text-align:center;
			background-color:#F5B800;
			padding-left:10px;
			padding-right:10px;
        }

        #zitat p{
            vertical-align:middle;
			padding-top: 15%;
			font-size: 3em;
        }

        #bild{
            position:absolute;
            top:58%;
            left:67%;
            width:33%;
            height:25%;
        }

        #bild_rand{
            position:absolute;
            top:82%;
            left:67%;
            width:33%;
            height:3%;
            background-color:#FFFF3D;
        }

        #bio{
            position:absolute;
            top:0%;
            left:67%;
            width:calc(33% - 10px);
            height:calc(32% - 10px);
			padding: 5px;
			color: white;
        }

        #bio_rand{
            position:absolute;
            top:32%;
            left:67%;
            width:33%;
            height:3%;
            background-color:#F5B800;
        }

        #literatur{
            position:absolute;
            top:85%;
            left:67%;
            width:calc(33% - 10px);
            height:calc(15% - 10px);
			padding: 5px;
			color: white;
        }

        #portrait{
            position:absolute;
            top:0%;
            left:22%;
            width:45%;
            height:100%;
        }

        #wirken{
            position:absolute;
            top:35%;
            left:67%;
            width:calc(33% - 10px);
            height:calc(20% - 10px);
			padding: 5px;
        }

        #wirken_rand{
            position:absolute;
            top:55%;
            left:67%;
            width:33%;
            height:3%;
            background-color:#700002;
        }

        #rahmen{
            position:absolute;
            top:0%;
            left:0%;
            width:100%;
            height:93%;
			border-bottom: solid red 10px;
        }

        #info{
            position:absolute;
            top:95%;
            left:0%;
            height:calc(5% - 10px);
            width:calc(100% - 10px);
			padding:5px;
			color:red;
        }
    </style>
<body>
<div id="rahmen">
    <div id="zitat"><p><?php echo $Zitat; ?></p></div>
    <div id="bild"><img style="width:100%; height:100%;" src="<?php echo $Bild2; ?>"/></div>
    <div id="bild_rand"></div>
    <div id="literatur" style="background-color:#000270;">
		<?php
			$u = 0;
			for($u = 0; $u < count($Literatur); $u++) {
				echo $Literatur[$u]."<br/>";
			}
		?>
	</div>
    <div id="bio" style="background-color:#000270;"><?php echo $Biok; ?></div>
    <div id="bio_rand"></div>
    <div id="wirken" style="background-color:#0044CC;"><?php echo $Wirkenk; ?></div>
    <div id="wirken_rand"></div>
    <div id="portrait"><img style="width:100%; height:100%;" src="<?php echo $Bild1; ?>"/></div>
</div>
<div id="info"><?php echo $Name." *".$Gdatum." in ".$Gort." &dagger;".$Tdatum." in ".$Tort; ?></div>
</body>
</html>