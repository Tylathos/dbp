<?php
$dbCon = new mysqli("localhost", "it", "neketa47", "vimu");

// Check connection
if ($dbCon->connect_error) {
    die("Connection failed: " . $dbCon->connect_error);
}
$dbCon->query("SET NAMES 'utf8'");
$number=$_GET["id"];
$res = $dbCon->query("SELECT * FROM gruppen WHERE Gid= '$number' LIMIT 1") or die("Error: " . $dbCon->error);
if ($res->num_rows > 0) {
		while($row = $res->fetch_assoc()) {
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Virtuelles Museum - Kategorie</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js">
	/* activate sidebar */
		$('#sidebar').affix({
			offset: {
			top: 235
			}
		});

	/* activate scrollspy menu */
		var $body   = $(document.body);
		var navHeight = $('.navbar').outerHeight(true) + 10;

		$body.scrollspy({
		target: '#leftCol',
		offset: navHeight
		});

	/* smooth scrolling sections */
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
			$('html,body').animate({
			scrollTop: target.offset().top - 50
			}, 1000);
			return false;
			}
			}
		});
	</script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<style>
	body{ 
	background-image: url(http://www.aids.org.za/wp-content/uploads/2013/08/background.png);
	}
	</style>
  </head>
  <body>
<?php include('menu.php');?>

<div id="masthead">  
  <div class="container">
      <div class="row">
        <div class="col-md-7">
          <h1><?php echo $row['Name']; ?>
          </h1>
        </div>
        <div class="col-md-5">
            <div class="well well-lg"> 
              <div class="row">
                <div class="col-sm-6">
        	      	<img src="//placehold.it/180x100" class="img-responsive">
                </div>
                <div class="col-sm-6">
	              	z.B. Jahreszahlen, wichtigste Vertreter. Bild kann wichtigsten Vertreter zeigen.
                </div>
              </div>
            </div>
        </div>
      </div> 
  </div><!--/container-->
</div><!--/masthead-->

<!--main-->
<div class="container">
	<div class="row">
      <!--left-->
      <div class="col-md-3" id="leftCol">
        <ul class="nav nav-stacked" id="sidebar">
          <li><a href="#sec0">Beschreibung</a></li>
          <li><a href="#sec1">Wichtigste Errungenschaften</a></li>
          <li><a href="#sec2">Vertreter der Kategorie</a></li>
        </ul>
      </div><!--/left-->
      
      <!--right-->
      <div class="col-md-9">
        <h2 id="sec0">Beschreibung</h2>
        <p>
          <?php echo $row['Kurzbeschreibung']; ?></p>
        
        <hr>
        <p>
		<?php echo $row['Beschreibung']; ?>
		</p>
        
        <h2 id="sec1">Wichtigste Errungenschaften</h2>
        <p>
         <?php echo $row['Errungenschaften']; ?>
        </p>
        
        <hr>
        
        <h2 id="sec2">Vertreter der Kategorie</h2>
		<?php
		$sql= $dbCon->query("SELECT * FROM rpg WHERE Gid='$number'");
		$relation= $sql->fetch_assoc();
		if ($relation['Pid'] != 0){
			?>
        <p>
          Folgende Personen lassen sich der Kategorie zuordnen:
        </p>
		<br>
		<?php
			while ($relation){
				$sql1= $dbCon->query("SELECT Name, Gdatum, Tdatum, Pid FROM person WHERE Pid='$relation[Pid]'");
				$person= $sql1->fetch_assoc();
		?>
		<div>
		<a href="display.php?id=<?php echo $person['Pid']; ?>"><h4><?php echo $person['Name'];?></h4></a>  <br>  <h4> *<?php echo $person['Gdatum'];?> - <?php echo $person['Tdatum'];?></h4>
		</div>
		<hr>
		<?php
		$person = $sql1->fetch_assoc();	
		$relation= $sql->fetch_assoc();		
			}
			
			
		}else{
			
			echo "Zu dieser Idee sind keine Personen eingetragen";
		}
		?>
        </div><!--/right-->
  	</div><!--/row-->
</div><!--/container-->
  </body>
</html>
<?php }
}else{
	header('Location: home.php');
	die();
}
	?>