﻿<?php
$dbCon = new mysqli("localhost", "it", "neketa47", "vimu");

// Check connection
if ($dbCon->connect_error) {
    die("Connection failed: " . $dbCon->connect_error);
}
$dbCon->query("SET NAMES 'utf8'");
$res = $dbCon->query("SELECT * FROM ideen LIMIT 3") or die("Error: " . $dbCon->error);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Virtuelles Museum - Kategorie</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<style>
	body{ 
	background-image: url(http://www.aids.org.za/wp-content/uploads/2013/08/background.png);
	}
	</style>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
<?php include('menu.php');?>

<div class="container marketing">

<?php
	$row = $res->fetch_assoc();
    if ($row["Iid"] != 0){
	
		while ($row){
		?>
		<div class="row">
			<div class="col-md-4 text-center">
				<img class="img-circle" src="http://placehold.it/250x250">
				<h2><?php echo $row["Name"]; ?></h2>
				<p><a class="btn btn-default" href="explicitidea.php?id=<?php echo $row ["Iid"]; ?>">Idee zeigen »</a></p>
		</div>	
		<?php
		$row = $res->fetch_assoc();		
		}
	}?>

  <!-- Three columns of text below the carousel 
  <div class="row">
    <div class="col-md-4 text-center">
      <img class="img-circle" src="http://placehold.it/250x250">
      <h2>Idee 1</h2>
      <p>Bla Bla Bla</p>
      <p><a class="btn btn-default" href="explicitidea.php">Idee zeigen »</a></p>
    </div>
    <div class="col-md-4 text-center">
      <img class="img-circle" src="http://placehold.it/250x250">
      <h2>Idee 2</h2>
      <p>TextText TextText TextText TextText</p>
      <p><a class="btn btn-default" href="explicitidea.php">Idee zeigen »</a></p>
    </div>
    <div class="col-md-4 text-center">
      <img class="img-circle" src="http://placehold.it/250x250">
      <h2>Idee 3</h2>
      <p>Bla Bla Bla Bla Bla Bla</p>
      <p><a class="btn btn-default" href="explicitidea.php">Idee zeigen »</a></p>
    </div>
  </div><!-- /.row -->

</div><!-- /.container -->
  </body>
</html>